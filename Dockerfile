FROM docker.io/node:alpine

RUN mkdir /opt/matrix-presents
COPY . /opt/matrix-presents
WORKDIR /opt/matrix-presents
RUN yarn --network-timeout=100000 install \
 && ln -s /data/config.json /opt/matrix-presents/config.json
COPY docker-run.sh /docker-run.sh
VOLUME ["/data"]
EXPOSE 8080
ENTRYPOINT ["/docker-run.sh"]

